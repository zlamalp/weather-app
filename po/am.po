# Amharic translation for ubuntu-weather-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-weather-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-weather-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-04-12 12:07+0100\n"
"PO-Revision-Date: 2018-05-27 14:12+0000\n"
"Last-Translator: Samson <sambelet@yahoo.com>\n"
"Language-Team: Amharic <https://translate.ubports.com/projects/ubports"
"/weather-app/am/>\n"
"Language: am\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.15\n"
"X-Launchpad-Export-Date: 2017-05-01 06:30+0000\n"

#: ../app/components/DayDelegateExtraInfo.qml:52
msgid "Chance of precipitation"
msgstr "የ መዝነብ እድል"

#: ../app/components/DayDelegateExtraInfo.qml:59
msgid "Winds"
msgstr "ነፋስ"

#: ../app/components/DayDelegateExtraInfo.qml:68
msgid "UV Index"
msgstr "የ ፀሐይ ጨረር"

#: ../app/components/DayDelegateExtraInfo.qml:74
msgid "Pollen"
msgstr "ብናኝ"

#: ../app/components/DayDelegateExtraInfo.qml:81
msgid "Humidity"
msgstr "የ አየር እርጥበት"

#: ../app/components/DayDelegateExtraInfo.qml:88
msgid "Sunrise"
msgstr "የ ፀሐይ መውጫ"

#: ../app/components/DayDelegateExtraInfo.qml:95
msgid "Sunset"
msgstr "የ ፀሐይ መጥለቂያ"

#: ../app/components/HeadState/LocationsHeadState.qml:36
#: ../app/components/HeadState/MultiSelectHeadState.qml:41
#: ../app/ui/HomePage.qml:41
msgid "Locations"
msgstr "አካባቢ"

#: ../app/components/HeadState/MultiSelectHeadState.qml:32
msgid "Cancel selection"
msgstr "ምርጫውን መሰረዣ"

#: ../app/components/HeadState/MultiSelectHeadState.qml:46
msgid "Select All"
msgstr "ሁሉንም መምረጫ"

#: ../app/components/HeadState/MultiSelectHeadState.qml:58
msgid "Delete"
msgstr "ማጥፊያ"

#: ../app/components/HomePageEmptyStateComponent.qml:51
msgid "Searching for current location..."
msgstr "የ አሁኑን አካባቢ በ መፈለግ ላይ..."

#: ../app/components/HomePageEmptyStateComponent.qml:52
msgid "Cannot determine your location"
msgstr "ያሉበትን አካባቢ ማወቅ አልተቻለም"

#: ../app/components/HomePageEmptyStateComponent.qml:62
msgid "Manually add a location by swiping up from the bottom of the display"
msgstr "በ እጅ አካባቢ ይጨምሩ ማሳያውን ከ ታች ወደ ላይ በ መጥረግ"

#: ../app/components/HomeTempInfo.qml:108
msgid "Today"
msgstr "ዛሬ"

#: ../app/components/HomeTempInfo.qml:112
#, qt-format
msgid "updated %1 day ago"
msgid_plural "updated %1 days ago"
msgstr[0] "የ ተሻሻለው ከ %1 ቀን በፊት ነው"
msgstr[1] "የ ተሻሻለው ከ %1 ቀኖች በፊት ነው"

#: ../app/components/HomeTempInfo.qml:114
#, qt-format
msgid "updated %1 hour ago"
msgid_plural "updated %1 hours ago"
msgstr[0] "የ ተሻሻለው ከ %1 ሰአት በፊት ነው"
msgstr[1] "የ ተሻሻለው ከ %1 ሰአቶች በፊት ነው"

#: ../app/components/HomeTempInfo.qml:116
#, qt-format
msgid "updated %1 minute ago"
msgid_plural "updated %1 minutes ago"
msgstr[0] "የ ተሻሻለው ከ %1 ደቂቃ በፊት ነው"
msgstr[1] "የ ተሻሻለው ከ %1 ደቂቆች በፊት ነው"

#: ../app/components/HomeTempInfo.qml:118
msgid "updated recently"
msgstr "የ ተሻሻለው በ ቅርብ ጊዜ ነው"

#: ../app/components/ListItemActions/Remove.qml:26
msgid "Remove"
msgstr "ማስወገጃ"

#: ../app/components/LocationsPageEmptyStateComponent.qml:36
msgid "No locations found. Tap the plus icon to search for one."
msgstr "አካባቢ አልተገኘም: መታ ያድርጉ መደመሪያ ምልክት ላይ ፈልጎ ለ መጨመር"

#: ../app/components/NetworkErrorStateComponent.qml:48
msgid "Network Error"
msgstr "የ ኔትዎርክ ስህተት"

#: ../app/components/NetworkErrorStateComponent.qml:57
msgid "Please check your network settings and try again."
msgstr "እባክዎን የ ኔትዎርክ ማሰናጃውን ይመርምሩ እና እንደገና ይሞክሩ"

#: ../app/components/NetworkErrorStateComponent.qml:63
msgid "Retry"
msgstr "እንደገና ይሞክሩ"

#: ../app/components/NoAPIKeyErrorStateComponent.qml:47
msgid "No API Keys Found"
msgstr "ምንም የ API ቁልፎች አልተገኙም"

#: ../app/components/NoAPIKeyErrorStateComponent.qml:56
msgid ""
"If you are a developer, please see the README file for instructions on how "
"to obtain your own Open Weather Map API key."
msgstr ""
"እርስዎ አበልፃጊ ከሆኑ: እባክዎን የ አንብቡኝ ፋይልን ይመልከቱ: ለ መመሪያ እርስዎ የ ራስዎትን የ አየር ንብረት ካርታ "
"API ቁልፍ እንደሚያገኙ:"

#: ../app/ubuntu-weather-app.qml:235
msgid ""
"One arguments for weather app: --display, --city, --lat, --lng They will be "
"managed by system. See the source for a full comment about them"
msgstr ""
"የ አየር ንብረት ክርክር መተግበሪያ: --ያሳያል: --ከተማ: --ላቲ: --ሎን የሚተዳደሩት በ ስርአቱ ነው: ለ ሙሉ "
"አስተያየት ምንጩን ይመልከቱ:"

#: ../app/ui/AddLocationPage.qml:38
msgid "Select a city"
msgstr "ከተማ ይምረጡ"

#: ../app/ui/AddLocationPage.qml:49
msgid "City"
msgstr "ከተማ"

#: ../app/ui/AddLocationPage.qml:65
msgid "Back"
msgstr "ወደ ኋላ"

#: ../app/ui/AddLocationPage.qml:108
msgid "Search city"
msgstr "ከተማ መፈለጊያ"

#: ../app/ui/AddLocationPage.qml:281
msgid "No city found"
msgstr "ምንም ከተማ አልተገኘም"

#: ../app/ui/AddLocationPage.qml:294
msgid "Couldn't load weather data, please try later again!"
msgstr "የ አየር ንብረት ዳታ መጫን አልተቻለም፡ እባክዎን እንደገና ይሞክሩ!"

#: ../app/ui/AddLocationPage.qml:304
msgid "Location already added."
msgstr "አካባቢው ቀደም ብሎ ተጨምሯል"

#: ../app/ui/AddLocationPage.qml:307
msgid "OK"
msgstr "እሺ"

#: ../app/ui/LocationsPage.qml:99
msgid "Current Location"
msgstr "የ አሁኑ አካባቢ"

#: ../app/ui/SettingsPage.qml:27
msgid "Settings"
msgstr "ማሰናጃ"

#: ../app/ui/SettingsPage.qml:44 ../app/ui/settings/UnitsPage.qml:29
msgid "Units"
msgstr "መለኪያ"

#: ../app/ui/SettingsPage.qml:52 ../app/ui/settings/DataProviderPage.qml:28
msgid "Data Provider"
msgstr "ዳታ አቅራቢው"

#: ../app/ui/SettingsPage.qml:60 ../app/ui/settings/RefreshIntervalPage.qml:27
msgid "Refresh Interval"
msgstr "ማናቃቂያ ከ እረፍት"

#: ../app/ui/SettingsPage.qml:68 ../app/ui/settings/LocationPage.qml:26
msgid "Location"
msgstr "አካባቢ"

#: ../app/ui/settings/DataProviderPage.qml:42
msgid "Provider"
msgstr "አቅራቢው"

#: ../app/ui/settings/LocationPage.qml:34
msgid "Detect current location"
msgstr "የ አሁኑን አካባቢ መፈለጊያ"

#: ../app/ui/settings/RefreshIntervalPage.qml:34
#: ../app/ui/settings/RefreshIntervalPage.qml:35
#: ../app/ui/settings/RefreshIntervalPage.qml:36
#: ../app/ui/settings/RefreshIntervalPage.qml:37
#: ../app/ui/settings/RefreshIntervalPage.qml:48
#, qt-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 ደቂቃ"
msgstr[1] "%1 ደቂቃዎች"

#: ../app/ui/settings/RefreshIntervalPage.qml:47
msgid "Interval"
msgstr "ክፍተት"

#. TRANSLATORS: The strings are standard measurement units
#. of temperature in Celcius and are shown in the settings page.
#. Only the abbreviated form of Celcius should be used.
#: ../app/ui/settings/UnitsPage.qml:47 ../app/ui/settings/UnitsPage.qml:83
msgid "°C"
msgstr "°ሴ"

#. TRANSLATORS: The strings are standard measurement units
#. of temperature in Fahrenheit and are shown in the settings page.
#. Only the abbreviated form of Fahrenheit should be used.
#: ../app/ui/settings/UnitsPage.qml:52 ../app/ui/settings/UnitsPage.qml:84
msgid "°F"
msgstr "°ፋ"

#. TRANSLATORS: The strings are standard measurement units
#. of wind speed in kilometers per hour and are shown in the settings page.
#. Only the abbreviated form of kilometers per hour should be used.
#: ../app/ui/settings/UnitsPage.qml:63 ../app/ui/settings/UnitsPage.qml:104
msgid "kph"
msgstr "ኪሜ/በ"

#. TRANSLATORS: The strings are standard measurement units
#. of wind speed in miles per hour and are shown in the settings page.
#. Only the abbreviated form of miles per hour should be used.
#: ../app/ui/settings/UnitsPage.qml:68 ../app/ui/settings/UnitsPage.qml:105
msgid "mph"
msgstr "ማ/ሰ"

#: ../app/ui/settings/UnitsPage.qml:82
msgid "Temperature"
msgstr "የ አየር ንብረት"

#: ../app/ui/settings/UnitsPage.qml:103
msgid "Wind Speed"
msgstr "የ ንፋስ ፍጥነት"

#: ubuntu-weather-app.desktop.in.in.h:1
msgid "Weather"
msgstr "የ አየር ንብረት"

#: ubuntu-weather-app.desktop.in.in.h:2
msgid ""
"A weather forecast application for Ubuntu with support for multiple online "
"weather data sources"
msgstr "ለ ኡቡንቱ የ አየር ንብረት መተግበሪያ በርካታ የ አየር ንብረት ዳታ ምንጮች በመስመር ላይ ይደገፋል"

#: ubuntu-weather-app.desktop.in.in.h:3
msgid "weather;forecast;twc;openweathermap;the weather channel;"
msgstr "የ አየር ንብረት: ግምት: twc;openweathermap የ አየር ንብረት ጣቢያ;"
